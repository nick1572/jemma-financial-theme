<div class="search-box">
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<div class="search-inner">
			<label class="screen-reader-text" for="s">Search for:</label>
			<button type="submit"><i class="fa fa-search"></i></button>
			<input type="hidden" name="" value="" />
			<input type="search" value="" id="searchsubmit" placeholder="...Search" name="s" id="s" class="jemma-search"/>
		</div>
	</form>
</div>