<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Financial
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">


		<?php
			/*if ( is_single() ) {
				echo '<div class="content-full-column">';
				echo '<div class="content-author-image">'. get_avatar( get_the_author_meta( 'ID' ), 125) . '</div>';
				echo '<div class="content-author-info">';
				the_title( '<h1 class="entry-title">', '</h1>' );
				echo 'By: ' . get_the_author(); the_author_meta( 'name');
				echo '<br>';
				echo  the_author_meta( 'occupation_profile').'</div>';
				echo '</div>';
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			/*}*/

    ?>

		<?php  
		if ( is_single() ) :
			    the_title( '<h1 class="entry-title">', '</h1>' );
			    jemma_fin_posted_on();
		else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;
		?>



	</header><!-- .entry-header -->

		<?php
		   if (is_page() || is_single() ) {
		   	    echo '<div class="entry-content">';
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'jemma_fin' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );

				/*wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jemma_fin' ),
					'after'  => '</div>',
				) );*/
				echo '</div><!-- .entry-content -->';

			} elseif (is_home() || is_category('be-informed')) {
				echo '<div class="entry-summary">';

					the_excerpt();
				echo '</div">';

			}
		?>



	<footer class="entry-footer">
		<?php jemma_fin_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
