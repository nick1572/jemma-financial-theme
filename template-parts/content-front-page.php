<div class="front-post-container">
	<div class="front-post">
		<div class="front-thumb">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
		</div>
		
		<div class="front-post-content">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php the_excerpt();?>
		</div>
	</div>
</div>