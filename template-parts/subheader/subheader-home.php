<?php
	$image_path = wp_upload_dir();
?>
<div class="site-hero">
		<div class="hero-inner" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/home-header-new.jpg)">
			<div class="hero-left">
				<div class="hero-button top-btn"><a href="<?php echo get_page_link(21);?>">How We Work with You <i class="fa fa-caret-right"></i></a></div>
				<div class="hero-button middle-btn"><a href="<?php echo get_page_link(92);?>">Speak With A Financial Advisor <i class="fa fa-caret-right"></i></a></div>
				<div class="hero-button second-mid-btn"><a href="<?php echo get_page_link(266);?>">Explore Our Financial Wisdom <i class="fa fa-caret-right"></i></a></div>
				<div class="hero-button bottom-btn"><a href="#signup-popup" class="popup-link">Sign Up For Our Updates <i class="fa fa-caret-right"></i></a></div>
			</div>
		</div>
</div>
