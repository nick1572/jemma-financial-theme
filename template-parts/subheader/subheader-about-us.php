<?php
	$image_path = wp_upload_dir();
?>
<div class="site-hero">
	<div class="hero-interior-inner">
		<div class="inner-div">
			<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/about-us-bg-header.jpg)">
			<h1><?php 
					if(is_page('about-us')) {
						echo get_the_title(); 
					} elseif (in_category('the-jemma-team')) {
						echo 'The Jemma Team';
					} elseif (is_404()) {
						esc_html_e( 'Oops! That page can&rsquo;t be found.', 'jemma_fin' );
					}
				?></h1>
			</div>
		</div>
		</div>
	</div>
