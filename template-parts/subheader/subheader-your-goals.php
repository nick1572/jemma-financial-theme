<?php
	$image_path = wp_upload_dir();
?>


<div class="site-hero">
	<div class="hero-interior-inner">
		<div class="inner-div">

		<?php if(is_page('education')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/education-header.jpg)">
			<?php elseif(is_page('retirement')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/retirement-header.jpg)">
			<?php elseif(is_page('budgeting-for-savings')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/budgeting-for-savings-header.jpg)">
			<?php elseif(is_page('divorced-widowed')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/dw-header.jpg)">
			<?php elseif(is_page('changing-jobs')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/changing-jobs-header.jpg)">
			<?php elseif(is_page('major-purchases')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/major-purchases-header.jpg)">
			<?php elseif(is_page('insurance')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/insurance-header.jpg)">
			<?php elseif(is_page('protect-yourself')): ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/protect-yourself-header.jpg)">
			<?php else: ?>
				<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/your-goals-header.jpg)">
		<?php endif; ?>
		<h1><?php echo get_the_title(); ?></h1>
			</div>
		</div>
		</div>
	</div>
