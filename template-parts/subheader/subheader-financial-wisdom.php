<?php
	$image_path = wp_upload_dir();
?>
<div class="site-hero">
	<div class="hero-interior-inner">
		<div class="inner-div">
			<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/financial-wisdom-bgheader.jpg)">
				<h1>Financial Wisdom</h1>
			</div>
		</div>
	</div>
</div>
