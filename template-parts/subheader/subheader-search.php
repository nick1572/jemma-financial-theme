<?php
	$image_path = wp_upload_dir();
?>
<div class="site-hero">
	<div class="hero-interior-inner">

		<div class="inner-div">
			<div class="about-us-gradient" style="background-image: url(<?php echo $image_path['baseurl']; ?>/headers/about-us-bg-header.jpg)">
			<h1>Search</h1>
			</div>
		</div>
		</div>
	</div>
