<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Financial
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php //jemma_fin_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php if (in_category('1')) { the_excerpt(); } else { echo '';} ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php jemma_fin_entry_footer(); ?>
	</footer><!-- .entry-footer -->
	<p><a href="" class="goback-baby" onClick="history.go(-1); return false;">Go Back &raquo;</a></p>
</article><!-- #post-## -->

