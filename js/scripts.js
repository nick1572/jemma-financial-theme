$(document).ready(function() {



    /*-----Simple Modal Popup-----*/

    $('.first-pop , a.show-link, .popup-link').magnificPopup({
        type: 'inline'
    });
    
    
    $('.popup-vimeo').magnificPopup({
        type: 'iframe',
        iframe: {
           markup: '<div class="mfp-iframe-scaler video-resize">'+
                        '<div class="mfp-close"></div>'+
                        '<iframe class="mfp-iframe" width="640" height="360" frameborder="0" allowfullscreen>            </iframe>'+
                    '</div>'
       }   
    });



    $(document).on('click', '.popup-modal-dismiss, .popup-modal-dismiss-button',  function(e) {
        var magnificPopup = $.magnificPopup.instance;
        e.preventDefault();
        magnificPopup.close();
    });


   $(document).on('click', '.closePopup', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
   });



    /*-----JQuery Accordion-----*/

    $("#accordion").accordion({
        collapsible: true,
        active: 'none',
        heightStyle: 'content',
        header: "div.accordionheader"
    });

    $("#field_abx5k, #field_abx5k2, #field_5631c").prepend("<option value='' selected='selected'>Select Your State</option>");

  

    /*-----Message Banner on Homepage-----*/


    /*var elem = $(".notify-banner");
    elem.fadeIn({
          duration: 1000,
          easing: "easeInOutQuad"
        });
   */

  
   
   /* var elem = $(".notify-banner");
    if (elem.length) {
      if (!sessionStorage.getItem('HasVisitedFirst')) { 
        elem.fadeIn({
          duration: 1000,
          easing: "easeInOutQuad"
        });
       sessionStorage.setItem('HasVisitedFirst',true);
      }
    }
    

    $('.banner-close').on('click', function() {
        $('.notify-banner').slideUp({
            duration: 1000,
            easing: 'easeOutQuad'
        });

    });*/

}); //---End on Ready