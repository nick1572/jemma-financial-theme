var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var sassbeautify = require('gulp-sassbeautify');

// Gulp Sass Task 
gulp.task('sass', function() {
  gulp.src('./sass/{,*/}*.{scss,sass}')
    .pipe(sourcemaps.init())
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'));
})

gulp.task('beautify-scss', function () {
   gulp.src('./sass/{,*/}*.{scss,sass}')
    .pipe(sassbeautify())
    .pipe(gulp.dest('./'));
})

gulp.task('default', ['sass'], function () {
  gulp.watch('./sass/{,*/}*.{scss,sass}', ['sass'])
});