<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Financial
 */

get_header(); ?>


	<div class="site-primary-content-area">
		<div class="primary">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );

					if(is_category('1')) {
						echo '<p>From 6 Ways to Save More Money for Retirement to 3 Ways to Build Your Net Worth Now we cover a wide range of topics and tips to keep you informed. Consider us your go to resource for all things financial.</p>';
					}

					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			//the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</div> <!--.site-primary-content-area -->
		<?php get_sidebar(); ?>
	</div><!-- .primary -->

<?php
get_sidebar();
get_footer();
