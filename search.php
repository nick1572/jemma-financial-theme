<?php

get_header(); ?>


	<div class="site-primary-content-area">
		<div class="primary">

		<?php

		/*$args = array( 'post_type' => 'post' );
		$args = array_merge( $args, $wp_query->query );
		query_posts( $args );*/


		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'jemma_fin' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			</header><!-- .page-header -->

			<?php

             

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

				//the_content();

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</div><!-- .primary -->
	<?php get_sidebar(); ?>
</div><!--.site-primary-content-area -->
<?php

get_footer();