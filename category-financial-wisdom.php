<?php
/**
* The template for displaying archive pages.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package Jemma_Financial
*/
get_header(); ?>
<div class="site-primary-content-area">
	<div class="primary">

		<?php
		if ( have_posts() ) : ?>
		<header class="page-header">
			<?php

				/*echo '<h1>';
				printf( __( '%s', 'jemma_fin' ), single_cat_title( '', false ) );
				echo '</h1>';
				*/
				

				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>

			<p class="financial-wisdom-search-intro">Search within Financial Wisdom Library</p>
			<div class="search-box">
				<form role="search" method="get" id="searchform-2" action="<?php echo home_url( '/' ); ?>">
					<div class="search-inner smaller-input">
						<label class="screen-reader-text" for="s">Search for:</label>
						<button type="submit"><i class="fa fa-search"></i></button>
						<input type="hidden" name="cat" value="6" />
						<input type="search" value="" id="searchsubmit2" placeholder="...Search" name="s" id="s" class="jemma-search"/>
					</div>
				</form>
			</div>
		</header><!-- .page-header -->
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
				/*
				* Include the Post-Format-specific template for the content.
				* If you want to override this in a child theme, then include a file
				* called content-___.php (where ___ is the Post Format name) and that will be used instead.
				*/
				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;
			//the_posts_navigation();
			wp_paginate();
			else :
			get_template_part( 'template-parts/content', 'none' );
			endif; ?>
			</div> <!--.site-primary-content-area -->
			<?php get_sidebar(); ?>
			</div><!-- .primary -->
			<?php
			get_sidebar();
			get_footer();
