<?php
	/*
		Template Name: Page Template
	*/

global $post;
get_header(); ?>
<main class="main">
<div class="content">
   
   
  <?php //$args  = array ('post__in' => array( 1015, 1109, 1340, 1000, 735, 1422));
	    //$query = new WP_Query( $args ); ?>
	
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php get_template_part( 'template-parts/content', 'page' );?>
	<?php endwhile; else : ?>
	<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
   
</div>
</main>
<?php get_footer(); ?>