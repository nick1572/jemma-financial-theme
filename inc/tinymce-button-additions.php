<?php

function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {  

// Define the style_formats array

	$style_formats = array(  
		 
		array(  
			'title' => 'Purple Text',  
			'block' => 'span',  
			'classes' => 'purple-text',
			'wrapper' => true,
		),
		array(  
			'title' => 'Deep Purple Text',  
			'block' => 'span',  
			'classes' => 'deep-purple-text',
			'wrapper' => true,
		),
		array(  
			'title' => 'Blue Text',  
			'block' => 'span',  
			'classes' => 'blue-text',
			'wrapper' => true,
		),
		array(  
			'title' => 'Smaller Title Text',  
			'block' => 'span',  
			'classes' => 'small-title-text',
			'wrapper' => true,
		),
		array(  
			'title' => 'Larger Title Text',  
			'block' => 'p',  
			'classes' => 'large-title-text'
		),

		array(  
			'title' => 'Retirement Icon',  
			'block' => 'span',  
			'classes' => 'icon-jemma-financial-chair',
			'wrapper' => true,
		),

		array(  
			'title' => 'Education Icon',  
			'block' => 'span',  
			'classes' => 'icon-jemma-financial-grad',
			'wrapper' => true,
		),

		array(  
			'title' => 'Goals Icon',  
			'block' => 'span',  
			'classes' => 'icon-jemma-financial-bullseye',
			'wrapper' => true,
		),






		
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 
?>