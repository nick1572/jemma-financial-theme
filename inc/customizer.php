<?php
/**
 * Jemma Financial Theme Customizer.
 *
 * @package Jemma_Financial
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function jemma_fin_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->add_setting( 'jem_fin_logo' );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'jem_fin_logo', array(
        'label'    => __( 'Upload Logo (replaces text)', 'jemFinancial' ),
        'section'  => 'title_tagline',
        'settings' => 'jem_fin_logo',
    ) ) );

}
add_action( 'customize_register', 'jemma_fin_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function jemma_fin_customize_preview_js() {
	wp_enqueue_script( 'jemma_fin_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'jemma_fin_customize_preview_js' );

