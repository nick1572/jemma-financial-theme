<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jemma_Financial
 */

?>

	</div><!-- #content -->
</main><!-- main-->
</div><!-- #page -->

	<div class="signup-row">
		<div class="signup-inner">

				<?php dynamic_sidebar( 'signup-row' ); ?>

		</div>
	</div>
	<footer class="site-footer" role="contentinfo">
		<div class="colophon">
			<?php if (is_active_sidebar( 'first-footer-widget-area')
				    && is_active_sidebar( 'second-footer-widget-area')
				    && is_active_sidebar( 'third-footer-widget-area')
				    && is_active_sidebar( 'fourth-footer-widget-area')
				) : ?>

		<?php endif ; ?>
		<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
		<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
		<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
		<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
		</div><!-- .site-info -->
		<div class="copyright">
			<p>Jemma Financial Services | Copyright <?php the_date('Y') ;?></p>
		</div>
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
