<?php
/**
 * Jemma Financial functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Jemma_Financial
 */

if ( ! function_exists( 'jemma_fin_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function jemma_fin_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Jemma Financial, use a find and replace
	 * to change 'jemma_fin' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'jemma_fin', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'jemma_fin' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'jemma_fin_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'jemma_fin_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function jemma_fin_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'jemma_fin_content_width', 640 );
}
add_action( 'after_setup_theme', 'jemma_fin_content_width', 0 );


/**
 * Add additional User Custom Fields
 */
function modify_user_methods($profile_fields) {

	$profile_fields['occupation'] = 'Occupation';


	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_user_methods');


// Trying to solve the child pages/header

/*function has_parent($post, $post_id) {
  if ($post->ID == $post_id) return true;
  else if ($post->post_parent == 0) return false;
  else return has_parent(get_post($post->post_parent),$post_id);
}*/



/**
 * Remove empty P tags in the editor
 *
 *
 */
function remove_empty_p( $content ) {
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

/**
 * Add Class to the body tag
 *
 *
 */
function add_slug_body_class( $classes ) {
		global $post;
		if ( isset( $post ) ) {
		$classes[] = $post->post_name;
	}
	return $classes;
	}
add_filter( 'body_class', 'add_slug_body_class' );

/**
 * Add Class to menu so we can have
 * a poup  from the menu
 *
 */
function add_menuclass($ulclass) {
	return preg_replace('/<a rel="menu-pop"/', '<a rel="menu-pop" class="first-pop"', $ulclass, 1);
		}
add_filter('wp_nav_menu','add_menuclass');



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function jemma_widgets_init() {
		register_sidebar(array(
			'name'          => 'Sidebar',
			'id'            => 'sidebar',
			'before_widget' => '<div class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="">',
			'after_title'   => '</h2>'
		));
		register_sidebar(array(
			'name'          => 'Header Banner',
			'id'            => 'header-banner',
			'before_widget' => '<div id="%1$s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="">',
			'after_title'   => '</h2>'
		));
		register_sidebar(array(
			'name'          => 'Header Links',
			'id'            => 'header-links',
			'before_widget' => '<div id="%1$s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="">',
			'after_title'   => '</h2>'
		));
		register_sidebar(array(
			'name'          => 'Search',
			'id'            => 'search',
			'before_widget' => '<div class="search-box">',
			'after_widget'  => '</div>'
		));
		register_sidebar(array(
			'name'          => 'Sign Up Row',
			'id'            => 'signup-row',
			'before_widget' => '<div class="signup-banner">',
			'after_widget'  => '</div>'
		));
		//Footer Widgets
		register_sidebar( array(
			'name' => __( 'First Footer Widget Area' ),
			'id' => 'first-footer-widget-area',
			'description' => __( 'The first footer widget area'),
			'before_widget' => '<div id="%1$s" class="jemma-footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="jemma-footer-widget-title">',
			'after_title' => '</h5>',
		) );
		register_sidebar( array(
			'name' => __( 'Second Footer Widget Area' ),
			'id' => 'second-footer-widget-area',
			'description' => __( 'The first footer widget area'),
			'before_widget' => '<div id="%1$s" class="jemma-footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="jemma-footer-widget-title">',
			'after_title' => '</h5>',
		) );
		register_sidebar( array(
			'name' => __( 'Third Footer Widget Area' ),
			'id' => 'third-footer-widget-area',
			'description' => __( 'The first footer widget area'),
			'before_widget' => '<div id="%1$s" class="jemma-footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="jemma-footer-widget-title">',
			'after_title' => '</h5>',
		) );
		register_sidebar( array(
			'name' => __( 'Fourth Footer Widget Area' ),
			'id' => 'fourth-footer-widget-area',
			'description' => __( 'The first footer widget area'),
			'before_widget' => '<div id="%1$s" class="jemma-footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="jemma-footer-widget-title">',
			'after_title' => '</h5>',
			) );
		
	}
	add_action('widgets_init',  'jemma_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function jemma_fin_scripts() {
	wp_enqueue_style( 'jemma_fin-style', get_stylesheet_uri() );
	//wp_enqueue_style( 'datepicker', get_template_directory_uri() . '/css/datepicker.css' , '4.5.2');
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js', array(), null, true );
	wp_enqueue_script( 'jemma_fin-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'jemma_fin-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_register_script( 'jquery-ui.min', get_template_directory_uri() . '/js/jquery-ui.min.js', array(), null, true );
	wp_register_script( 'jquery.magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.js', array(), null, true );
	wp_register_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', array(), null, true );

	wp_enqueue_script('jquery');
	wp_enqueue_script('jquery-ui.min');
	wp_enqueue_script('jquery.magnific-popup');
	wp_enqueue_script('scripts');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'jemma_fin_scripts' );

/**
 * Implement Tiny MCE Button Custom Additions
 */
require get_template_directory() . '/inc/tinymce-button-additions.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


//Control Excerpt Length when using the_excerpt

 function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


//Using the content more....this will change (more..) to Read More

/*function modify_read_more_link() {
    return '<a class="readmore more-link" href="' . get_permalink() . '"> Read More &raquo;</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );*/


// Replaces the excerpt "Read More" text by a link


function new_excerpt_more($more) {
    global $post;
	return '...<a class="readmore" href="'. get_permalink($post->ID) . '"> Read More &raquo;</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');





  function my_post_queries( $query ) {
          // only homepage and is the main query
          if ($query->is_home() && $query->is_main_query()){
             // display only posts in category with slug 'articles'
             $query->set('category_name', 'be-informed');
             // avoid sticky posts
             $query->set('post__not_in', get_option( 'sticky_posts' ) );
          }
        }
        add_action( 'pre_get_posts', 'my_post_queries' );

// Custom walker to allow drop down menus to be in columns
class Column_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">";
		// Add this piece to start to wrap all the <li> tags in a <div class="col">
		if( $depth == 0 ) {
			$output .= "\n$indent<div class=\"col\">";
		}
		// End
		$output .= "\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		// Add this piece to close the wrap of all the <li> tags in a <div class="col">.
		// It should be noted that an extra empty <div> is included to allow for clearing any floated column.
		// Feel free to remove '$indent<div class=\"clear\"></div>' if you have your own clear method
		if( $depth == 0 ) {
			$output .= "$indent</div>\n$indent<div class=\"clear\"></div>";
		}
		// End
		$output .= "$indent</ul>\n";
	}
	function start_el(&$output, $item, $depth, $args) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' .  esc_attr( $class_names ) . '"';
		// This piece checks to see if you added a class of 'col-break' to any menu item in the admin area.
		// If you did, it breaks the column and starts a new one BEFORE the item is displayed
		if( stripos( $class_names, 'col-break' ) !== false )
			$output .= '</div><div class="col">';
		// End
		$output .= $indent . '<li id="menu-item-' .  $item->ID . '"' . $value . $class_names . '>';
		$attributes  = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}



