<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Jemma_Financial
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
 <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700" rel="stylesheet" type='text/css'>
<link href="<?php echo bloginfo('stylesheet_directory');?>/css/font-awesome-4.6.3/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

<?php wp_head(); ?>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-85540260-1', 'auto');
  ga('require', 'linkid');
  ga('send', 'pageview');

</script>

</head>

<body <?php body_class(); ?>>


<div id="leaving-popup" class="popup white-popup mfp-hide">
	<div class="popup-inner">
		<div class="pop-content">
			
			<div class="leaving-info">
			<h3 class="widget-title smaller-title">You are now leaving Jemma Financial</h3>
			<a class="btn-ctrl deep-purple warning-button" href="http://jemmaeveryday.com" target="_blank" onclick="$.magnificPopup.close();">Yes, I would like to leave</a>
			<a class="btn-ctrl deep-purple warning-button popup-modal-dismiss-button">No, I would like to stay</a>
		   </div>
		</div>
	</div>
</div>


<div id="leaving-popup2" class="popup white-popup mfp-hide">
	<div class="popup-inner">
		<div class="pop-content">
		
			<div class="leaving-info">
			<h3 class="widget-title smaller-title">You are now leaving Jemma Financial</h3>
			<a class="btn-ctrl deep-purple warning-button" href="https://client.schwab.com/Login/SignOn/CustomerCenterLogin.aspx?&kc=y&sim=y" target="_blank" onclick="$.magnificPopup.close();">Yes, I would like to leave</a>
			<a class="btn-ctrl deep-purple warning-button popup-modal-dismiss-button">No, I would like to stay</a>
	
			<p class="notice">Important Notice</p>
			<p>You are now leaving the Jemma Financial Services website and will be entering the Charles Schwab & Co., Inc. (“Schwab”) website.</p>
			<p>Schwab is a registered broker-dealer, and is not affiliated with Jemma Financial Services or any advisor(s) whose name(s) appear(s) on this website. Jemma Financial Services is/are independently owned and operated. [Schwab neither endorses nor recommends {Name(s) of Investment Management Firm(s)}, unless you have been referred to us through the Schwab Advisor Network®. (This bracketed language is for use by Schwab Advisor Network members only.)] Regardless of any referral or recommendation, Schwab does not endorse or recommend the investment strategy of any advisor. Schwab has agreements with “Name(s) of Firm(s)” under which Schwab provides Jemma Financial Services with services related to your account. Schwab does not review the Jemma Financial Services website(s), and makes no representation regarding information contained in the Jemma Financial Services website, which should not be considered to be either a recommendation by Schwab or a solicitation of any offer to purchase or sell any securities.</p>
		   </div>
		</div>
	</div>
</div>



<div id="signup-popup" class="popup white-popup mfp-hide">
		<div class="popup-inner">
			<div class="pop-content">
			
				<!--<div class="schwab-account-button"><a href="#">Login to your existing account</a></div>
                <div class="or">OR</div>-->
				<div class="get-started-form">
				
				<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 11, 'title' => false, 'description' => false ) ); ?>

				</div>
			</div>
		</div>
	</div>



	<div id="get-started-popup" class="popup white-popup mfp-hide">
		<div class="popup-inner">
			<div class="pop-content">
				
				<!--<div class="schwab-account-button"><a href="#">Login to your existing account</a></div>
                <div class="or">OR</div>-->
				<div class="get-started-form">
					<?php echo FrmFormsController::get_form_shortcode( array( 'id' => 4, 'title' => false, 'description' => false ) ); ?>
					<p>Need assistance? Call 855.662.2121 or email <a href="mailto:medwards@jemmafinancial.com">medwards@jemmafinancial.com</a></p>
				</div>
			</div>
		</div>
	</div>


<?php if (is_front_page() && is_active_sidebar('Header Banner')) : ?>
		<div class="notify-banner">
			<div class="banner-message">
			<?php dynamic_sidebar( 'header-banner' ); ?>
			</div>
		</div>
		<?php endif; ?>



<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'jemma_fin' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php $image_path = wp_upload_dir(); ?> <!--Did this work-->
				<div class="site-logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
	 					<img src="<?php echo $image_path['baseurl']; ?>/2016/08/jemma-fa-logo.svg" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
	 				</a>
 				</div>

			<?php
			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
			<div class="site-header-actions">
					<div class="site-header-search">

						<?php get_search_form(); ?>

					</div>
					<div class="site-header-links"><a href="<?php echo get_permalink(92);?>">Contact</a>  | <a href="#leaving-popup" class="popup-link">Go to Jemma Everyday</a></div>

			</div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'jemma_fin' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'walker' => new Column_Walker ) ); ?>
		</nav><!-- #site-navigation -->
<!--End-->
	</header><!-- #masthead -->



	<?php if(is_front_page()) {
			get_template_part( 'template-parts/subheader/subheader', 'home' );
		} elseif (is_page('about-us') || is_404() || in_category('8') || in_category(10)) {
			get_template_part( 'template-parts/subheader/subheader', 'about-us' );
		} elseif (in_category('1') || is_category('1')) {
			get_template_part( 'template-parts/subheader/subheader', 'be-informed' );
		} elseif (is_page('your-needs-our-solutions') || $post->post_parent == 22) {
			get_template_part( 'template-parts/subheader/subheader', 'your-goals' );
		} elseif (in_category('6') || $post->post_parent == 266){
			get_template_part( 'template-parts/subheader/subheader', 'financial-wisdom' );
		} elseif (is_page('contact')) {
			get_template_part( 'template-parts/subheader/subheader', 'contact' );
		}  elseif (is_search()) {
			get_template_part( 'template-parts/subheader/subheader', 'search' );
		} elseif (is_page('glossary')) {
			get_template_part( 'template-parts/subheader/subheader', 'glossary' );
		}



	?>


	<main id="main" class="site-main">
		<div id="content" class="site-content">
