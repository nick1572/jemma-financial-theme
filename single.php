<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Jemma_Financial
 */

get_header(); ?>

	<div class="site-primary-content-area">

		<div class="primary">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );


					echo '<hr/><div class="article-disclosure">No content published here constitutes a recommendation of any particular investment, security, portfolio of securities, transaction or investment strategy. To the extent any of the content published may be deemed to be investment advice, such information is impersonal and not tailored to the investment needs of any specific person. Consult your advisor about what is best for you.</div>';

					/*if (in_category('be-informed')) {



					$post_id = $post->ID; // current post id
					$cat = get_the_category();
					$current_cat_id = $cat[0]->cat_ID; // current category Id

					$args = array('category'=>$current_cat_id,'orderby'=>'post_date','order'=> 'DESC');
					$posts = get_posts($args);
					// get ids of posts retrieved from get_posts
					$ids = array();
					foreach ($posts as $thepost) {
		    		$ids[] = $thepost->ID;
					}
					// get and echo previous and next post in the same category
					$thisindex = array_search($post->ID, $ids);
					$previd = $ids[$thisindex-1];
					$nextid = $ids[$thisindex+1];
					echo '<div class="prev-next">';
					if (!empty($previd)){
					?>
					<a rel="prev" class="prev" href="<?php echo get_permalink($previd) ?>">Previous Article</a>
					<?php
					}
					if (!empty($nextid)){
					?>
					<a rel="next" class="next" href="<?php echo get_permalink($nextid) ?>">Next Article</a>
					<?php
					}
					echo '</div>';

				}*/
			


		endwhile; // End of the loop.


		?>
		<!--<p><a href="" class="goback-baby" onClick="history.go(-1); return false;">Go Back &raquo;</a></p>-->


		</div><!-- .primary -->
		<?php get_sidebar(); ?>
	</div><!-- .site-primary-content-area -->
<?php
get_sidebar();
get_footer();
