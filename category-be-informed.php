<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jemma_Financial
 */

get_header(); ?>


	<div class="site-primary-content-area">
		<div class="primary">
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php

				//echo '<h1>';
				//printf( __( '%s', 'jemma_fin' ), single_cat_title( '', false ) );
				//echo '</h1>';
				echo '<p>Jemma Financial covers a wide range of topics and tips to keep you informed. Consider us your "go-to" resource for all things financial.</p>';
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;
           
			


		else :
            echo '<p>Jemma Financial covers a wide range of topics and tips to keep you informed. Consider us your "go-to" resource for all things financial.</p>
			';
			echo '		<p><a href="" class="goback-baby" onClick="history.go(-1); return false;">Go Back &raquo;</a></p>
			';
			//get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		 <?php wp_paginate(); ?>

		</div> <!--.site-primary-content-area -->
		<?php get_sidebar(); ?>
	</div><!-- .primary -->

<?php
get_sidebar();
get_footer();
